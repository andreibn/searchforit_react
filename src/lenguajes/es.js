const es = {
    inicio: 'Inicio',
    ult_noticias: "últimas noticias",
    objetivos: "Objetivos",
    contactanos: "Contáctanos",
    sobre: "sobre nosotros:",
    noticias_destacadas: "NOTICIAS DESTACADAS",
    consiste: "¿En qué consiste?",
    que_es: "¿Qué es?",
    a_que_nos_dedicamos: "¿A qué nos dedicamos?",
    acerca_de: "Acerca de",
    contacto1: "Direccion y Contacto",
    contacto2: "Añadir comentario",
    mensaje: "mensaje",
    enviar: "enviar"
}
export default es;
const pt = {
    inicio: 'Inicio',
    ult_noticias: "Notícias mais recentes",
    objetivos: "Objectives",
    contactanos: "Contate-Nos",
    sobre: "sobre nos:",
    noticias_destacadas: "NOTICIAS DESTACADAS",
    consiste: "¿O qué consiste?",
    que_es: "¿O q es?",
    a_que_nos_dedicamos: "¿O que nós fazemos?",
    acerca_de: "Sobre",
    contacto1: "Endereço e contato",
    contacto2: "Añadeur o Comenteur",
    mensaje: "mensaje",
    enviar: "enviar"
}
export default pt;
const en = {
    inicio: 'Home',
    ult_noticias: "Latest News",
    objetivos: "Objetives",
    contactanos: "Contact Us",
    sobre: "about us:",
    noticias_destacadas: "FEATURED NEWS",
    consiste: "What does it consist of?",
    que_es: "What is it?",
    a_que_nos_dedicamos: "What do we do?",
    acerca_de: "About us",
    contacto1: "Direction and contact",
    contacto2: "Add comment",
    mensaje: "message",
    enviar: "send"
}

export default en;
function Objetivo({titulo,contenido,id}){
    const nameItem='seccion_objetivos-contenedor--'+id;
    return(
        <>
            <div className={nameItem}>
                <h3 className="seccion_objetivos-contenedor-titulo">{titulo}</h3>
                <p className="seccion_objetivos-contenedor-texto">{contenido}</p>
            </div>
        </>
    )
}
export default Objetivo;
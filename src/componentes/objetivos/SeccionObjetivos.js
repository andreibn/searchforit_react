import Objetivo from './Objetivo.js';
function SeccionObjetivos({listaObjetivos}){
    return(
        <>
        <div className="seccion_objetivos">
          <div className="seccion_objetivos-titulo">Nuestros objetivos</div>
          <div className="seccion_objetivos-contenedores">
            {listaObjetivos.map(objetivo=><Objetivo key={objetivo.id} titulo={objetivo.titulo} contenido={objetivo.contenido} id={objetivo.id}/>)}
          </div>
          </div>
        </>
    )
}

export default SeccionObjetivos;
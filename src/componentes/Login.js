import React, { useState } from 'react';

export const  Login = (props) => {   
var usernames = ['andrei','alex','jaime','pedro'];
var passwords = ['andrei','alex','jaime','pedro'];
var admins = ['alex','andrei'];
const [logInText, setLogInText] = useState("");
const [loggedIn, setLoggedIn] = useState(false);

function checkCredentials(){
    var user = document.getElementById('username').value;
    var pass = document.getElementById('password').value;
    if(usernames.includes(user)){
        if(pass === passwords[passwords.indexOf(user)]){
            if(admins.includes(user)){
                setLogInText("bienvenido, admin "+user);
                setLoggedIn(true);
                props.setIsAdmin(true);
            }else{
                setLogInText("bienvenido "+user);
                setLoggedIn(true);
            }
        }else{
            setLogInText("contraseña incorrecta");
        }
    }else{
        setLogInText("usuario incorrecto");
    }
}
    return (
     <div>
         <form className="login"> 
             {
                loggedIn ? null : (<>
                <input type="text" id="username"  placeholder="username" required/>
                <input type="password" id="password"  placeholder="password" required/>
                <input type="submit" id="login_button" value="login" onClick={() =>  checkCredentials() }/>
                </>)

            }
            <h1 className="login_text">{logInText}</h1>
         </form>
     </div>
    );        
   

}
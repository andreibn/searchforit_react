import Noticia1 from '../img/noticias/Noticia1.jpg';
import Noticia2 from '../img/noticias/Noticia2.jpg';
import Noticia3 from '../img/noticias/Noticia3.jpg';
import Noticia4 from '../img/noticias/Noticia4.jpg';
import Noticia5 from '../img/noticias/Noticia5.jpg';
import Noticia6 from '../img/noticias/Noticia6.jpg';
import Noticia7 from '../img/noticias/Noticia7.jpg';
import Noticia8 from '../img/noticias/Noticia8.jpg';

const noticias = [
    Noticia1,
    Noticia2,
    Noticia3,
    Noticia4,
    Noticia5,
    Noticia6,
    Noticia7,
    Noticia8
]
export default noticias;
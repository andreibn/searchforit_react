import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBook, faHome, faInfo } from '@fortawesome/free-solid-svg-icons';
import { faDiscord } from '@fortawesome/free-brands-svg-icons';

export const MenuLateral = (props) => {

  return (
    <div  className = {props.abierto ? 'menu_lateral-visible' : 'menu_lateral-oculto'}>
      <ul className="menu_lateral-ul">
        <li className="menu_lateral-li"><a href="#section" className="menu_lateral-a"><FontAwesomeIcon icon={faHome} className="menu_lateral-icono" />Inicio</a></li>
        <li className="menu_lateral-li"><a href="#section" className="menu_lateral-a"><FontAwesomeIcon icon={faBook} className="menu_lateral-icono" />Guía</a></li>
        <li className="menu_lateral-li"><a href="#section" className="menu_lateral-a"><FontAwesomeIcon icon={faInfo} className="menu_lateral-icono" />Info</a></li>
        <li className="menu_lateral-li"><a href="#section" className="menu_lateral-a"><FontAwesomeIcon icon={faHome} className="menu_lateral-icono" />Donar</a></li>
        <li className="menu_lateral-li"><a href="#section" className="menu_lateral-a"><FontAwesomeIcon icon={faDiscord} className="menu_lateral-icono" />Discord</a></li>
      </ul>
    </div>
  );
}
import React from "react";
import {useForm} from "react-hook-form";

export const Formulario=({setInfoComentarios})=>{
    const {register,handleSubmit}=useForm();
    const onSubmit=(data)=>{
        setInfoComentarios(prevdata=>[...prevdata,data]);
    }
    return(
    <div className="footer_formulario">
        <form onSubmit={handleSubmit(onSubmit)}>
          <label className="footer_formulario-label">Nombre *</label>
          <hr/>
          <input type="text" name="autor" required className="footer_formulario-inputEmail" ref={register} />
          <hr/>
          <label className="footer_formulario-label">Mensaje *</label>
          <hr/>
          <textarea rows='2' cols="25" required name="contenido" className="footer_formulario-textArea" ref={register} ></textarea>             
          <hr/>
          <button type="submit" className="footer_formulario-boton">Enviar</button>       
        </form>
    </div>
    );
}
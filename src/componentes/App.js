import React, { useState, useEffect } from 'react';
import '../sass/App.scss';
import gif from '../gif/ezgif.com-gif-maker.mp4';
import { MenuLateral } from './MenuLateral';
import { BotonMenuLateral } from './BotonMenuLateral';
import { Noticias } from './Noticias';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faInstagram, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faEnvelopeOpen, faMapMarkerAlt, faPhone } from '@fortawesome/free-solid-svg-icons';
import SeccionObjetivos from './objetivos/SeccionObjetivos.js';
import Comentarios from './comentarios/Comentarios.js';
import {ComentarioContext} from './comentarios/ComentarioContext.js';
import counterpart from 'counterpart'; 
import Translate from 'react-translate-component';
import es from '../lenguajes/es'
import en from '../lenguajes/en'
import pt from '../lenguajes/pt'
import { Bandera } from './Banderas'
import { Login } from './Login'
import {Formulario} from './addComentario/Formulario.js';
//librería react-translate-component doc: https://www.npmjs.com/package/react-translate-component
//<a className="menu_lateral-boton" href="#section" onClick={() => this.Ocultar()}>Click</a>

export const App = () => {
  //funcion para cargar contenidos del json-server
  const getAsyncDataFromFakeAPI=(API_ENDPOINTER,isLoadingSetter,infoSetter)=>{
    isLoadingSetter(true);
    fetch(API_ENDPOINTER).then(response=>response.json()).then(result=>{
      isLoadingSetter(false);
      infoSetter(result);
    })
  }; 
  //Objetivos loader:
  const [infoObjetivos,setInfoObjetivos]=React.useState([]);
  const [isLoading,setIsLoading]=React.useState(false);


  //Comentarios loader:
  const [isLoadingComentarios,setIsLoadingComentarios]=React.useState(false);
  const [infoComentarios,setInfoComentarios]=React.useState([]);


  //Se realiza una vez se inicie la págian
  React.useEffect(()=>{
    getAsyncDataFromFakeAPI("http://localhost:3001/objetivos",setIsLoading,setInfoObjetivos);
    getAsyncDataFromFakeAPI("http://localhost:3001/comentarios",setIsLoadingComentarios,setInfoComentarios);
  },[]);


  //Funcion para eliminar comentario->Usuario Corriente:
  const handleRemoveComentario=comentario=>setInfoComentarios(infoComentarios.filter(com=>com.autor!==comentario.autor));

  //traduccion a idiomas
  const [localLanguage, setCurrentLocalLanguage] = useState("es");

  useEffect(() => {
    counterpart.registerTranslations('es',es);
    counterpart.registerTranslations('en',en);
    counterpart.registerTranslations('pt',pt);
    counterpart.setLocale("es");
  }, []);
  
  function changeLanguage(lang)
  {
    counterpart.setLocale(lang);
    setCurrentLocalLanguage(lang);
  }

  //boton abrir menu

  const [menuAbierto, setMenuAbierto] = useState(false);

  //admin
  const [isAdminLoggedIn, setIsAdminLoggedIn] = useState(false);

    return (
      <div>
        <header className="header">
        <BotonMenuLateral abrirMenu={setMenuAbierto} abierto={menuAbierto}/>
          <div className="header_enlaces">
            <Login setIsAdmin={setIsAdminLoggedIn}/>
            <ul className="header_enlaces-ul">
              <li className="header_enlaces-li"><a href="#seccion-Inicio" className="header_enlaces-a"><Translate content="inicio"/></a></li>
              <li className="header_enlaces-li"><a href="#seccion-Noticias" className="header_enlaces-a"><Translate content="ult_noticias"/></a></li>
              <li className="header_enlaces-li"><a href="#seccion-Objetivos" className="header_enlaces-a"><Translate content="objetivos"/></a></li>
              <li className="header_enlaces-li"><a href="#seccion-Contactanos" className="header_enlaces-a"><Translate content="contactanos"/></a></li>
              <i><Bandera lang='es' onClick={changeLanguage} isLocalLanguage={localLanguage === 'es'}/></i>
              <i><Bandera lang='en' onClick={changeLanguage} isLocalLanguage={localLanguage === 'en'} /></i>
              <i><Bandera lang='pt' onClick={changeLanguage} isLocalLanguage={localLanguage === 'pt'}/></i>
            </ul>
          </div>
        </header>
        
      <MenuLateral abierto={menuAbierto}/>
          <a name="seccion-Inicio" href="#section"></a>
          <div className="portada">
            <div className="portada_texto">
              <h1 className="portada_texto-titulo">SearchForIt</h1>
              <div className="portada_texto-contenedor-boton"><a className="portada_texto-boton" href="#seccion-Informacion"><Translate content="sobre"/></a></div>
            </div>
          </div>
          <a name="seccion-Noticias" href="#section"></a>
          <div className="noticias">
            <div className="noticias-titulo-contenedor">
              <h2 className="noticias-titulo"><Translate content="noticias_destacadas"/></h2>
            </div>
            <div className="noticias_destacadas-contenedor">
              <div className="noticias_destacadas-superior">
                <Noticias imagen="1" titulo="Empiezan las elecciones presidenciales en EEUU 2020" />
                <Noticias imagen="2" titulo="Se aprueba el Estado de Alarma a partir del 14 de marzo de 2020 en España" />
                <Noticias imagen="3" titulo="Joe Biden gana las elecciones presidenciales de EEUU" />
                <Noticias imagen="4" titulo="Los casos de coronavirus bajan y disminuye la presión hospitalaria" />
              </div>
              <div className="noticias_destacadas-inferior">
                <Noticias imagen="5" titulo="La NASA confirma la existencia de agua en la superficie de la Luna" />
                <Noticias imagen="6" titulo="Fallece el futbolista Diego Armando Maradona por una insuficiencia cardíaca aguda" />
                <Noticias imagen="7" titulo="Las nuevas vacunas del COVID-19 podrían llegar a principios de 2021" />
                <Noticias imagen="8" titulo="Sanidad impone el confinamiento perimetral a Madrid y otros municipios" />
              </div>
            </div>
          </div>
          <a name="seccion-Informacion" href="#section"></a>
          <div className="seccion_informacion">
            <div className="seccion_informacion-titulo-contenedor">
              <h2 className="seccion_informacion-titulo"><Translate content="consiste"/></h2>
            </div>
            <div className="seccion_informacion-contenedor">
              <div className="seccion_informacion-contenedor-texto">
                <div className="seccion_informacion-texto">
                  <h2 className="seccion_informacion-texto-titulo"><Translate content="que_es"/></h2>
                  <p className="seccion_informacion-texto-texto">Lorem ipsum dolor sit amet, consectetur adipisicing elit dicta illo eveniet ullam. Fuga suscipit ipsam distinctio saepe officia possimus repellendus quaerat animi non optio exercitationem, dolores unde. Lorem ipsum dolor sit amet, consectetur adipisicing elit dicta illo eveniet ullam. Fuga suscipit ipsam distinctio saepe officia possimus repellendus quaerat animi non optio exercitationem, dolores unde.</p>
                </div>
                <div className="seccion_informacion-texto">
                  <h2 className="seccion_informacion-texto-titulo"><Translate content="a_que_nos_dedicamos"/></h2>
                  <p className="seccion_informacion-texto-texto">Lorem ipsum dolor sit amet, consectetur adipisicing elit dicta illo eveniet ullam. Fuga suscipit ipsam distinctio saepe officia possimus repellendus quaerat animi non optio exercitationem, dolores unde. Lorem ipsum dolor sit amet, consectetur adipisicing elit dicta illo eveniet ullam. Fuga suscipit ipsam distinctio saepe officia possimus repellendus quaerat animi non optio exercitationem, dolores unde.</p>
                </div>
              </div>
              <div className="seccion_informacion-imagen">
                <video className='gif_noticias' autoPlay loop muted>
                  <source src={gif} type='video/mp4' />
                </video>
              </div>
            </div>
          </div>
          <a name="seccion-Objetivos" href="#section"></a>
          {
            isLoading?
            <p>Cargando onjetivos...</p>
              :
            <SeccionObjetivos listaObjetivos={infoObjetivos}/>
          }
          <ComentarioContext.Provider value={{
            onRemoveComentario:handleRemoveComentario,
            isAdmin : isAdminLoggedIn

          }}>
          {
            isLoadingComentarios?
            <p>Cargando comentarios...</p>
              :
              <>
              {infoComentarios.length===0?
              <div className="emptyComentarios">
                <h1 className="emptyComentarios-texto">Se han eliminado todos los comentarios</h1>
              </div>
              :
              <Comentarios listadoComentarios={infoComentarios}/>
              }
              </>
          }
          </ComentarioContext.Provider>
        
        <footer className="footer">
          <div className="footer-seccion1">
            <h2 className="footer-titulo"><Translate content="acerca_de"/></h2>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis eos quae, dolores facilis nesciunt fuga aperiam est corrupti numquam necessitatibus, amet nostrum ipsa autem modi voluptatibus. Saepe sit iure blanditiis!</p>
            <div className="footer-redesSociales">
              <ul className="footer_redesSociales-ul">
                <li className="footer_redesSociales-li">
                  <FontAwesomeIcon icon={faInstagram} className="footer_redesSociales-iconoInstagram" />
                </li>
                <li className="footer_redesSociales-li">
                  <FontAwesomeIcon icon={faFacebook} className="footer_redesSociales-iconoFacebook" />
                </li>
                <li className="footer_redesSociales-li">
                  <FontAwesomeIcon icon={faTwitter} className="footer_redesSociales-iconoTwitter" />
                </li>
              </ul>
            </div>
          </div>
          <div className="footer-seccion2">
            <h2 className="footer-titulo"><Translate content="contacto1"/></h2>
            <ul className="footer_contacto-ul">
              <li className="footer_contacto-li"><FontAwesomeIcon icon={faMapMarkerAlt} className="footer_contacto-icono" />Mahadahonda, Madrid</li>
              <li className="footer_contacto-li"><FontAwesomeIcon icon={faPhone} className="footer_contacto-icono" />+34 916-435-534</li>
              <li className="footer_contacto-li"><FontAwesomeIcon icon={faEnvelopeOpen} className="footer_contacto-icono" />searchforit@gmail.com</li>
            </ul>
          </div>
          <div className="footer-seccion3">
            <h2 className="footer-titulo"><Translate content="contacto2"/></h2>
            <div className="footer_formulario">
              <Formulario setInfoComentarios={setInfoComentarios}/>
            </div>
          </div>

        </footer>
      </div>
    );
}

import React from 'react'
import bandera from './BanderasLenguajes';

export const  Bandera = (props) => {   
    var lenguaje;
    if( props.lang === 'es' )
    {
        lenguaje = 0; //español
    }
    else if( props.lang === 'en' )
    {
        lenguaje = 1; //inglés
    }
    else if ( props.lang === 'pt' )
    {
        lenguaje = 2; //portugués
    }

    return (
     <div>
         <img src={bandera[lenguaje]} alt={props.lang} width="40px" className = {props.isLocalLanguage ? 'active' : null} height="30px" lang={props.lang} onClick={() => { props.onClick(props.lang)}} />
     </div>
    );        
   

}
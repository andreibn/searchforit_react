import Comentario from './Comentario.js';
function Comentarios({listadoComentarios}){
    const ListaComentarios=()=>{
        return(
            listadoComentarios.map(comentario=><Comentario key={comentario.autor} comentario={comentario}/>)
        );
    }
    return(
        <>
        <div className="comentarios">
            <div className="comentarios-titulo-contenedor">
                <h2 className="comentarios-titulo">Esta es la seccion comentarios</h2>
            </div>
            <div className="contenedor_comentarios">
                <ListaComentarios/>
            </div>
        </div>
        </>
    );
}
export default Comentarios;

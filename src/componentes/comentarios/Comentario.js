import React from 'react';
import {ComentarioContext} from './ComentarioContext.js';
function Comentario({comentario}){
    const {autor,contenido}=comentario;
    const {isAdmin} = React.useContext(ComentarioContext);
    const {onRemoveComentario}=React.useContext(ComentarioContext);
    return(
        <div className="caja_comentario">
            <h3 className="caja_comentario-titulo">Comentario publicado por:{autor}</h3>
            <p className="caja_comentario-contenido">{contenido}</p>
            {isAdmin ? <button className="caja_comentario-boton" type="button" onClick={()=>onRemoveComentario(comentario)}>Ocultar Comentario D:</button> : null}
        </div>
    );
}
export default Comentario;
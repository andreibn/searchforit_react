import bandera_es from '../img/idiomas/idioma_españa.png'
import bandera_en from '../img/idiomas/idioma_ingles.png'
import bandera_pt from '../img/idiomas/idioma_portugal.jpg'
const banderas =  [
    bandera_es,
    bandera_en,
    bandera_pt,
]
export default banderas;